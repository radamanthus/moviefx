Path For Migrating MovieFX To Trailblazer

[✔] Add the trailblazer-rails gem to Gemfile
[ ] Refactoring
  [✔] Rename SavedConfiguration AR model to Screen
  [✔] Rename SavedConfiguration effect column to screen_type
  [✔] Rename Effects:: Controllers to Screen:: Controllers
  [✔] Remove SlugGenerator and just use SecureRandom.urlsafe_base64(n)
[ ] Add System tests
  [✔] Manual test: Show a screen
  [✔] Automate test: Show a screen
    [✔]  Add a Processing screen fixture
  [✔] Automate the CreateScreen test
  [✔] Automate the UpdateScreen test
[✔] Basic refactorings on Screen::Base to simplify code
[ ] Move the logic inside Screen::BaseControllers to operations
  [ ] Create the CreateScreen operation
  [ ] Move the logic from Screen::Base#create to CreateScreen
  [ ] Create the UpdateScreen operation
  [ ] Move the logic from Screen::Base#update to UpdateScreen
