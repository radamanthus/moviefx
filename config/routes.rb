Rails.application.routes.draw do
  root 'home#index'

  namespace :screens do
    resources :processing
    resources :access
    resources :clock
  end
end
