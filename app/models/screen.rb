# frozen_string_literal: true

class Screen < ApplicationRecord
  validates :slug, uniqueness: true
end
