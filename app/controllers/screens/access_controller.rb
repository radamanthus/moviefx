class Screens::AccessController < Screens::BaseController
  helper_method :bgcolor

  private

  def bgcolor
    '#ffffff'
  end

  def configuration_from_params_and_defaults # rubocop:disable Metrics/AbcSize
    {
      header_text: sanitize(params[:header_text]),
      mode: sanitize(params[:mode]),
      username_label: sanitize_or_default(:username_label),
      password_label: sanitize_or_default(:password_label),
      username_placeholder: sanitize_or_default(:username_placeholder),
      login_button_label: sanitize_or_default(:login_button_label),
      access_denied_text: sanitize_or_default(:access_denied_text),
      access_granted_text: sanitize_or_default(:access_granted_text),
      correct_password: sanitize_or_default(:correct_password),
      max_retries: sanitize_or_default(:max_retries),
      redirect_delay: params[:redirect_delay].to_i || default_config[:redirect_delay],
      success_redirect_url: params[:success_redirect_url] || default_config[:success_redirect_url],
      failure_redirect_url: params[:failure_redirect_url] || default_config[:failure_redirect_url],
    }
  end

  def default_config
    {
      username_label: 'Username',
      password_label: 'Password',
      username_placeholder: 'Type your username here',
      login_button_label: 'Login',
      access_denied_text: 'Access Denied',
      access_granted_text: 'Access Granted',
      correct_password: '123qaz',
      max_retries: 3,
      redirect_delay: 0,
    }
  end

  def sanitize_or_default(key)
    sanitize(params[key]) || default_config[key]
  end

  def screen_type
    'access'
  end

  def route
    'access'
  end

  def session_saved_config
    Screen.find_by(session_id: request.session.id)
  end
end
