class Screens::BaseController < ApplicationController
  include ActionView::Helpers::SanitizeHelper

  before_action :set_screen_from_slug, only: %i[edit show]
  before_action :set_screen_from_id, only: %i[update]

  helper_method :bgcolor

  def create
    result = Screen::Operation::Create.call(
      {
        session_id: request.session.id,
        screen_type: screen_type,
        configuration: configuration_from_params_and_defaults,
      }
    )
    # TODO: Handle the failure result
    redirect_to edit_path(result[:screen])
  end

  def edit
    permalink = "/screens/#{route}/#{@screen.slug}"
    edit_url = "#{permalink}/edit"
    render locals: {
      id: @screen.id,
      permalink: permalink,
      edit_url: edit_url,
      config: @screen.configuration.with_indifferent_access,
    }
  end

  def new
    render locals: { config: Screen.new(configuration: default_config).configuration.with_indifferent_access }
  end

  def show
    render layout: 'screens', locals: { config: @screen.configuration.with_indifferent_access }
  end

  def update
    @screen.configuration = configuration_from_params_and_defaults
    @screen.save!
    redirect_to show_path(@screen)
  end

  private

  def edit_path(saved_config)
    public_send("edit_screens_#{route}_path", { id: saved_config.slug })
  end

  def show_path(saved_config)
    public_send("screens_#{route}_path", { id: saved_config.slug })
  end

  def set_screen_from_id
    @screen = Screen.find(params[:id])
  end

  def set_screen_from_slug
    @screen = Screen.find_by(slug: params[:id])
  end
end
