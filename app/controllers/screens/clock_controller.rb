class Screens::ClockController < Screens::BaseController
  helper_method :bgcolor

  private

  def bgcolor
    '#1b1c1d'
  end

  def configuration_from_params_and_defaults
    {
    }
  end

  def default_config
    {
    }
  end

  def screen_type
    'clock'
  end

  def route
    'clock'
  end

  def session_saved_config
    Screen.find_by(session_id: request.session.id)
  end
end
