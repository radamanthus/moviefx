module Screen::Operation
  class Create < Trailblazer::Operation
    step :generate_slug
    step :save_screen

    def generate_slug(ctx, secure_random: SecureRandom, **)
      ctx[:slug] = secure_random.urlsafe_base64(12)
    end

    def save_screen(ctx, session_id:, screen_type:, configuration:, **)
      begin
        screen = Screen.create!(
          session_id: session_id,
          screen_type: screen_type,
          configuration: configuration,
          slug: ctx[:slug]
        )
      rescue ActiveRecord::RecordInvalid
        ctx[:error] = "Slug #{ctx[:slug]} is already taken."
        return false
      end

      ctx[:screen] = screen
    end
  end
end
