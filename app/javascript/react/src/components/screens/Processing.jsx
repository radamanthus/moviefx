import * as React from "react"
import { createRoot } from 'react-dom/client'
import { Button, Container, Header, Modal, Progress, Segment } from 'semantic-ui-react'

class Processing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      maxProgress: parseInt(props.maxProgress) || 100,
      processingText: props.processingText,
      progress: parseInt(props.startPosition) || 0,
      progressBarColor: props.progressBarColor,
      startButtonLabel: props.startButtonLabel,
      stepDelay: parseInt(props.stepDelay) || 1000,
      stepIncrement: parseInt(props.stepIncrement) || 1,
      timerStarted: false 
    }
  }

  componentDidMount() { 
    this.timerID = setInterval(
      () => this.updateProgress(),
      this.state.stepDelay
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  startTimer() {
    this.setState({ timerStarted: true });
  }

  updateProgress() {
    if (!this.state.timerStarted) {
      return;
    }
    if (this.state.progress >= this.state.maxProgress) {
      return;
    }

    var new_progress = this.state.progress + this.state.stepIncrement;
    this.setState({ progress: new_progress });
  }

  render() {
    return (
      <div>
        <Container>
          <Modal trigger={<Button onClick={this.startTimer.bind(this)}>{this.state.startButtonLabel}</Button>}>
            <Segment inverted>
              <Progress percent = {this.state.progress} progress inverted color={this.state.progressBarColor}>
                <Header inverted size='medium'>{this.state.processingText}</Header>
              </Progress>
            </Segment>
          </Modal>
        </Container>
      </div>
    );
  }
}

export default Processing
