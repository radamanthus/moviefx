import * as React from "react"
import { createRoot } from 'react-dom/client'

class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      interval: props.interval,
    };
    console.log(this.state);
  }

  componentDidMount() { 
    this.timerID = setInterval(
      () => this.tick(),
      1000 * this.state.interval
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  render() {
    return (
      <div>
        {this.state.date.toLocaleTimeString()}
      </div>
    );
  }
}

export default Clock
