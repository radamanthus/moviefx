import { define } from 'remount'

import Access from './components/screens/Access'
import Processing from './components/screens/Processing'
import Clock from './components/screens/Clock'

define({
  'effects-access': {
    component: Access,
    attributes: [
      'headerText',
      'mode',
      'usernameLabel',
      'passwordLabel',
      'loginButtonLabel',
      'accessDeniedText',
      'accessGrantedText',
      'correctPassword',
      'maxRetries',
      'successRedirectUrl',
      'failureRedirectUrl',
      'redirectDelay',
      'userNameInput',
      'passwordInput',
      'loginResultText',
      'loginResultIcon'
    ]
  },
  'effects-processing': {
    component: Processing,
    attributes: [
      'maxProgress',
      'processingText',
      'progress',
      'progressBarColor',
      'startButtonLabel',
      'stepDelay',
      'stepIncrement'
    ],
  },
  'effects-clock': {
    component: Clock,
    attributes: ['interval']
  }
})
