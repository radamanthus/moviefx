class CreateScreens < ActiveRecord::Migration[7.0]
  def change
    create_table :screens do |t|
      t.string      :screen_type, null: false
      t.string      :slug
      t.string      :hashed_password
      t.string      :session_id
      t.jsonb       :configuration, null: false, default: '{}'
      t.timestamps
    end

    add_index :screens, :slug, unique: true
  end
end
