require 'application_system_test_case'

class ScreensTest < ApplicationSystemTestCase
  setup do
    @screen = Screen.first
  end

  test 'Showing a processing screen' do
    visit screens_processing_path(@screen.slug)

    assert_selector 'h1', text: 'Are you sure?'
  end

  test 'Creating a new processing screen' do
    visit new_screens_processing_path
    fill_in 'start_text', with: 'Do you really want to launch the nuke?'
    click_on 'Save'

    assert_selector 'h1', text: 'Your progress bar has been saved to the Movie Effects Library'

    new_screen = Screen.order(created_at: :desc).first
    assert_equal 'Do you really want to launch the nuke?', new_screen.configuration['start_text']
  end

  test 'Updating a processing screen' do
    visit edit_screens_processing_path(@screen.slug)
    fill_in 'start_text', with: 'Are you sure you want to download the virus?'
    click_on 'Save'

    assert_selector 'h1', text: 'Your progress bar has been saved to the Movie Effects Library'
    updated_screen = @screen.reload
    assert_equal 'Are you sure you want to download the virus?', updated_screen.configuration['start_text']
  end
end
