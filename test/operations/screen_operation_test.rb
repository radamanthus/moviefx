require 'test_helper'

class ScreenOperationTest < Minitest::Spec
  describe 'Screen::Operation::Create' do
    class NotRandom # rubocop:disable Lint/ConstantDefinitionInBlock
      def self.urlsafe_base64(*)
        "42isAlwaysTheAnswer"
      end
    end

    it 'creates a screen with a unique slug' do
      screen_data = {
        session_id: 'asdf',
        screen_type: 'processing',
        configuration: {},
      }

      result = Screen::Operation::Create.wtf?(screen_data)
      assert result.success?
    end

    it 'fails when trying to insert the same slug twice' do
      screen_data = {
        session_id: 'asdf',
        screen_type: 'processing',
        configuration: {},
        secure_random: NotRandom,
      }

      result = Screen::Operation::Create.wtf?(screen_data)
      assert result.success?
      assert_equal '42isAlwaysTheAnswer', result[:slug]

      result = Screen::Operation::Create.wtf?(screen_data)
      assert result.failure?
      assert_equal 'Slug 42isAlwaysTheAnswer is already taken.', result[:error]
    end
  end
end
